package ar.com.bitman.yasqt.test.model.query;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.config.ConfigManager;
import ar.com.bitman.yasqt.model.query.DbConnection;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class DbConnectionTest
{
    @Test
    public void testConstructorNotNull()
            throws YASQTException
    {
        assertNotNull(new DbConnection("", "", "", "", ConfigManager.getInstance()));
    }

    @Test
    public void testDriverCantConnectAndFails()
            throws YASQTException
    {
        try
        {
            DbConnection conn = new DbConnection("cstr1", "", "", "db1", ConfigManager.getInstance());
            conn.setDriver(new DriverCantConnectMock());
            conn.getConnection();
            fail("Connection must fail");
        } catch (YASQTException e)
        {
            assertEquals(YASQTException.Type.CONNECTION, e.getType());
        }
    }

    @Test
    public void testDriverCanConnectOK()
            throws YASQTException
    {
        DbConnection conn = new DbConnection(DriverCanConnectMock.CONNECTION_STRING, "", "", "db2", ConfigManager.getInstance());
        conn.setDriver(new DriverCanConnectMock());
        Connection connection = conn.getConnection();

        // Just to be sure that the right driver/connection was retrieved
        assertEquals(DriverCanConnectMock.CONNECTION_ID, connection.toString());
    }
}
