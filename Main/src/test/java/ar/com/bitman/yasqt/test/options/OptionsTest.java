package ar.com.bitman.yasqt.test.options;


import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.options.Options;
import ar.com.bitman.yasqt.options.ValidOptions;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class OptionsTest {

    public static final String THIS_URL = "THIS_URL";
    public static final String URL_CMD = "--url";
    public static final String USER = "juanYpinchame";
    public static final String USER_CMD = "--user";

    @Test
    public void testArgumentSetting()
            throws YASQTException
    {
        String[] args = new String[]{URL_CMD, THIS_URL, USER_CMD, USER};
        Options opt = new Options(args);
        opt.parse();
        assertEquals(THIS_URL, opt.getArg(ValidOptions.URL,0));
        assertEquals(USER, opt.getArg(ValidOptions.USER,0) );
    }
}
