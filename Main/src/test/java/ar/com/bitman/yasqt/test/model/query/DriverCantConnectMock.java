package ar.com.bitman.yasqt.test.model.query;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;


public class DriverCantConnectMock
        implements Driver
{
    @Override
    public Connection connect(String url, Properties info)
            throws SQLException
    {
        throw new SQLException();
    }

    @Override
    public boolean acceptsURL(String url)
            throws SQLException
    {
        return (url.equals("cstr1"));
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info)
            throws SQLException
    {
        return new DriverPropertyInfo[0];
    }

    @Override
    public int getMajorVersion()
    {
        return 0;
    }

    @Override
    public int getMinorVersion()
    {
        return 0;
    }

    @Override
    public boolean jdbcCompliant()
    {
        return true;
    }

    @Override
    public Logger getParentLogger()
            throws SQLFeatureNotSupportedException
    {
        throw new SQLFeatureNotSupportedException();
    }
}
