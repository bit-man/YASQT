package ar.com.bitman.yasqt.test.config;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.config.JdbcProperties;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class JdbcPropertiesTest {

    private static Set<String> validDbs = new HashSet<>();
    public static final String INVALID_DATABASE_NAME = "invalid database name";

    public static final String MYSQL = "mysql";
    public static final String MSSQL = "mssql";

    static {
        validDbs.add("mysql");
        validDbs.add("mssql");
    }

    @Test
    public void testSingleton()
            throws YASQTException
    {
        JdbcProperties prop = JdbcProperties.getInstance();
        JdbcProperties prop2 = JdbcProperties.getInstance();
        assertTrue("Singleton mechanism is not working", prop == prop2);
    }

    @Test
    public void testInvalidDBName() {
        try {
            JdbcProperties.getInstance().getDriver(INVALID_DATABASE_NAME);
            fail("Invalid database name was accepted");
        } catch (YASQTException e) {
            // It's OK
        }
    }

    @Test
    public void testDatabases()
            throws YASQTException
    {
        JdbcProperties prop = JdbcProperties.getInstance();

        for (String db : validDbs)
            assertTrue("Invalid database : " + db, prop.databases().contains(db));
    }

    @Test
    public void testDrivers()
            throws YASQTException
    {
        JdbcProperties prop = JdbcProperties.getInstance();

        for (String db : validDbs)
            prop.getDriver(db);
    }

    @Test
    public void testJars()
            throws YASQTException
    {
        JdbcProperties prop = JdbcProperties.getInstance();

        for (String db : validDbs)
            prop.getJarNames(db);
    }

    @Test
    public void testJarsDB()
            throws YASQTException
    {
        JdbcProperties prop = JdbcProperties.getInstance();

        List<String> mysqlJars = new ArrayList<>();
        mysqlJars.add("mysql-connector-java-5.1.10.jar");
        assertEquals(mysqlJars, prop.getJarNames(MYSQL));

        List<String> mssqlJars = new ArrayList<>();
        mssqlJars.add("mssqlserver.jar");
        mssqlJars.add("msbase.jar");
        mssqlJars.add("msutil.jar");
        mssqlJars.add("sqljdbc4.jar");
        assertEquals(mssqlJars, prop.getJarNames(MSSQL));
    }
}
