package ar.com.bitman.yasqt.test.main;


import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.SQLCommand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class SQLCommandTest {
    @Test
    public void testDDL()
            throws YASQTException
    {
        SQLCommand cmd = new SQLCommand("create sarasa sarasa");
        assertTrue( cmd.isDDL() );
        assertFalse( cmd.isDML() );
    }

    @Test
    public void testDML()
            throws YASQTException
    {
        SQLCommand cmd = new SQLCommand("insert sarasa sarasa");
        assertFalse( cmd.isDDL() );
        assertTrue( cmd.isDML() );
    }

    @Test
    public void testIsAnythingElse()
            throws YASQTException
    {
        SQLCommand cmd = new SQLCommand("sarasa sarasa");
        assertFalse( cmd.isDDL() );
        assertFalse( cmd.isDML() );
    }

    @Test
    public void testIsInvalidSQLCommand()
    {
        try
        {
            new SQLCommand("NoSQLCommandNoSpaces");
            fail("Invalid SQL sentence went undetected");
        } catch (YASQTException e)
        {

        }
    }
}
