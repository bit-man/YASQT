package ar.com.bitman.yasqt.test.main;

import ar.com.bitman.yasqt.commands.QueryFile;
import ar.com.bitman.yasqt.commands.SQLCommand;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static junit.framework.Assert.*;

public class QueryFileTest {
    @Test
    public void testEmptyFile() {
        QueryFile queryFile = new QueryFile("empty.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testSingleCommand() {
        QueryFile queryFile = new QueryFile("singleCmd.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testTwoCommands() {
        QueryFile queryFile = new QueryFile("twoSingleCommands.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someOtherTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testTwoCommandsPlusComment() {
        QueryFile queryFile = new QueryFile("twoSingleCommandsPlusComment.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someOtherTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());

    }


    @Test
    public void testReadingNextOnly() {
        QueryFile queryFile = new QueryFile("empty.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        try {
            iterator.next();
            fail("Empty file, should be no next element");
        } catch (NoSuchElementException e) {
            // It's OK
        }
    }


    @Test
    public void testSingleCommandInTwoLines() {
        QueryFile queryFile = new QueryFile("singleCmdTwoLines.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testTwoCommandsInThreeLines() {
        QueryFile queryFile = new QueryFile("twoSingleCommandsThreeLines.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someOtherTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testTwoCommandsInThreeLinesPlusComments() {
        QueryFile queryFile = new QueryFile("twoCmdsThreeLinesPlusComments.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable", sqlCommand.getSqlCmd());

        sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someOtherTable", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testSingleCommandThreeLines() {
        QueryFile queryFile = new QueryFile("singleCmdThreeLines.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable where someClauseIsTrue", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }


    @Test
    public void testSingleCommandFourLines() {
        QueryFile queryFile = new QueryFile("singleCmdFourLines.txt");
        Iterator<SQLCommand> iterator = queryFile.getIterator();

        assertTrue(iterator != null);
        assertTrue(iterator.hasNext());

        SQLCommand sqlCommand = iterator.next();
        assertTrue(sqlCommand != null);
        assertEquals("select * from someTable where someClauseIsTrue AND newClauseIsTrueAlso", sqlCommand.getSqlCmd());

        assertFalse(iterator.hasNext());
    }

}
