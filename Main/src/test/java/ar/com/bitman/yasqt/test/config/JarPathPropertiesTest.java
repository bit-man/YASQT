package ar.com.bitman.yasqt.test.config;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.config.JarPathProperties;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class JarPathPropertiesTest {


    @Test
    public void testSingleton()
            throws YASQTException
    {
        JarPathProperties prop = JarPathProperties.getInstance();
        JarPathProperties prop2 = JarPathProperties.getInstance();
        assertTrue("Singleton mechanism is not working", prop == prop2);
    }

    @Test
    public void testPaths()
            throws YASQTException
    {
        JarPathProperties prop = JarPathProperties.getInstance();
        List<File> jarPaths = new ArrayList<>();
        jarPaths.add( new File("/usr/share/java") );
        jarPaths.add( new File(System.getenv("HOME"), "lib") );
        assertEquals(jarPaths, prop.getPaths());
    }
}
                           