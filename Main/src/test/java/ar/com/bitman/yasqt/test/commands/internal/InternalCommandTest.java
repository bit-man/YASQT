package ar.com.bitman.yasqt.test.commands.internal;

import ar.com.bitman.yasqt.commands.internal.InternalCommandFactory;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.assertTrue;

public class InternalCommandTest {


    @Test
    public void testCommandRecognition() throws IOException {
        assertTrue(InternalCommandFactory.isCommand("shortcut-list"));
        assertTrue(!InternalCommandFactory.isCommand("NONEXISTENT-COMMAND"));
    }

}
