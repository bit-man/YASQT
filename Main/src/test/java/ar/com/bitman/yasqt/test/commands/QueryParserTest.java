package ar.com.bitman.yasqt.test.commands;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.CommandType;
import ar.com.bitman.yasqt.commands.QueryParser;
import ar.com.bitman.yasqt.config.ConfigManager;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

public class QueryParserTest {


    @Test
    public void testCommandRecognition()
            throws IOException, YASQTException
    {
        ConfigManager cfgMgr = ConfigManager.getInstance();
        QueryParser queryParser = new QueryParser(cfgMgr);

        Map<String, String> testShortcuts = new HashMap<>();
        testShortcuts.put("madeup-shortcut", "some command");

        cfgMgr.getQueryManager().load(testShortcuts);

        queryParser.parse("select * from pepe");
        assertEquals(CommandType.SQL_QUERY, queryParser.getCommandType() );

        queryParser.parse("shortcut-list");
        assertEquals(CommandType.INTERNAL, queryParser.getCommandType() );

        String firstShortcut = cfgMgr.getQueryManager().getShortcutList().iterator().next();
        queryParser.parse(firstShortcut);
        assertEquals(CommandType.SHORTCUT, queryParser.getCommandType());

        String testQryPath = "/tmp/test.qry";
        File testQry = new File(testQryPath);
        testQry.createNewFile();
        queryParser.parse(testQryPath);

        assertEquals(CommandType.FILE, queryParser.getCommandType());

        testQry.delete();
    }

}
