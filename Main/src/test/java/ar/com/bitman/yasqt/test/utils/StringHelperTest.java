package ar.com.bitman.yasqt.test.utils;

import ar.com.bitman.yasqt.utils.StringHelper;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;

public class StringHelperTest {
    @Test
    public void testRepeat() {
        assertEquals("----------", StringHelper.repeat('-', 10));
        assertEquals("--", StringHelper.repeat('-', 2));
        assertEquals("-", StringHelper.repeat('-', 1));
    }

    @Test
    public void testSubstring() {
        assertEquals( "lamb", StringHelper.substring("lamborghini",4));
    }
}
