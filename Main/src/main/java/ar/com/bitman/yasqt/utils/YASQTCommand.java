package ar.com.bitman.yasqt.utils;

import ar.com.bitman.yasqt.Logger;
import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.SQLCommand;
import ar.com.bitman.yasqt.model.Column;

import java.sql.Connection;

public class YASQTCommand
    extends SQLCommand {
    public YASQTCommand(String sqlCmd)
            throws YASQTException
    {
        super(sqlCmd);
    }

    public YASQTCommand(String sqlCmd, Connection conn)
            throws YASQTException
    {
        super(sqlCmd, conn);
    }

    public void displayColumnNames()  {
        for( Column column  : getColumns()) {
            Logger.logInfo(column.getName());
        }
    }

}
