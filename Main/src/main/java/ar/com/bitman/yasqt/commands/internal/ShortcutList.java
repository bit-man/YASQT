package ar.com.bitman.yasqt.commands.internal;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.Command;
import ar.com.bitman.yasqt.config.ConfigManager;

import java.util.Set;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 9/6/13
 * Time: 4:33 PM
 */
public class ShortcutList extends Command {
    private Set<String> shortcutList;

    @Override
    public void execute() throws YASQTException {
        try {
            shortcutList = ConfigManager.getInstance().getQueryManager().getShortcutList();
        } catch (Exception e) {
            throw new YASQTException(e, YASQTException.Type.UNKNOWN);
        }
    }

    @Override
    public void displayResults() throws YASQTException {
        System.out.println(shortcutList);
    }
}
