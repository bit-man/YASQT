package ar.com.bitman.yasqt.config;

import ar.com.bitman.yasqt.YASQTException;

import java.io.*;
import java.util.*;

import static ar.com.bitman.yasqt.config.Constants.*;

public class JdbcProperties {

    private static Map<String, List<String>> jars = new HashMap<>();
    private static Map<String, String> driver = new HashMap<>();
    private static JdbcProperties singleton = null;

    private JdbcProperties() {
    }

    public static JdbcProperties getInstance()
            throws YASQTException
    {
        if (singleton == null) {
            singleton = new JdbcProperties();
            singleton.load();
        }

        return singleton;
    }
    private void load()
            throws YASQTException
    {

        Properties bareProps = new Properties();
        try
        {
            bareProps.load(singleton.getClass().getResourceAsStream(JDBC_PROPERTIES));
        } catch (IOException e)
        {
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Can't read file " + Constants.JDBC_PROPERTIES, e);
        }

        for (String pName : bareProps.stringPropertyNames()) {
            String dbName = pName.substring(0, pName.indexOf(DOT));
            String value = bareProps.getProperty(pName);
            if (pName.endsWith(JARS)) {
                for( String jar : value.split(","))
                    addJarToDB(dbName, Tools.replaceVariables(jar));
            } else if (pName.endsWith(DRIVER))
                driver.put(dbName, value);
            else
                throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid properties format, Key: " + pName + ", Value: " + value);
        }

    }

    private void addJarToDB(String dbName, String value) {
        if (!jars.containsKey(dbName)) {
            List<String> l = new ArrayList<>();
            l.add(value);
            jars.put(dbName, l);
        } else {
            List<String> l = jars.get(dbName);
            l.add(value);
        }
    }

    public Set<String> databases() {
        return driver.keySet();
    }

    public String getDriver(String database)
            throws YASQTException
    {
        if (!databases().contains(database))
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid database name " + database);
        return driver.get(database);
    }


    public List<String> getJarNames(String database)
            throws YASQTException
    {
        if (!databases().contains(database))
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid database name " + database);
        return jars.get(database);
    }

}
