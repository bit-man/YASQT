package ar.com.bitman.yasqt.commands.internal;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.Command;
import ar.com.bitman.yasqt.config.ConfigManager;
import ar.com.bitman.yasqt.config.QueryProperties;

public class ShortcutShow
        extends Command
{


    private String shortcut;

    @Override
    public void execute() throws YASQTException {
        try {
            QueryProperties queryManager = ConfigManager.getInstance().getQueryManager();
            if (! queryManager.isShortcut(args))
                throw new YASQTException(YASQTException.Type.UNKNOWN, "Cannot find shortcut '" + args + "'" );
            else
                shortcut = queryManager.getShortcut(args);


        } catch (Exception e) {
            throw new YASQTException(e, YASQTException.Type.UNKNOWN);
        }
    }

    @Override
    public void displayResults() throws YASQTException {
        System.out.println(shortcut);
    }
}
