package ar.com.bitman.yasqt.commands;

import ar.com.bitman.yasqt.Constants;
import ar.com.bitman.yasqt.YASQTException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class QueryFile {
    private File file;

    public QueryFile(String filePath) {
        this.file = new File(filePath);
    }

    public Iterator<SQLCommand> getIterator() {
        return new QueryFileIterator<SQLCommand>();
    }

    private class QueryFileIterator<T> implements Iterator<SQLCommand> {
        private BufferedReader in;
        private String line;
        private boolean lineRead = false;

        public boolean hasNext() {
            try {
                advanceToNextUsableLine();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return (line != null);
        }

        /**
         * Skips comment lines, setting 'line' the next usable one
         *
         * @throws IOException
         */
        private void advanceToNextUsableLine() throws IOException {
            if (in == null)
                in = new BufferedReader(new FileReader(file));

            // Skip comments
            if ((line = in.readLine()) != null && line.startsWith(Constants.COMMENT_INDICATOR))
            {
                do
                    ;
                while ((line = in.readLine()) != null && line.startsWith(Constants.COMMENT_INDICATOR));
            }

            lineRead = true;
        }

        public SQLCommand next() {
            if (!lineRead)
                try {
                    advanceToNextUsableLine();
                } catch (IOException e) {
                    NoSuchElementException exc = new NoSuchElementException(e.getMessage());
                    exc.addSuppressed(e);
                    throw exc;
                }
            lineRead = false;

            if (line == null)
                throw new NoSuchElementException();
            else {
                String sqlCmd;
                try {
                    sqlCmd = obtainFullSQLCommand();
                } catch (YASQTException e) {
                    e.printStackTrace();
                    throw createException(e);
                }
                try
                {
                    return new SQLCommand(sqlCmd);
                } catch (YASQTException e)
                {
                    e.printStackTrace();
                    throw createException(e);
                }
            }
        }

        private NoSuchElementException createException(Throwable e) {
            NoSuchElementException exc = new NoSuchElementException();
            exc.initCause(e);
            return exc;
        }

        private String obtainFullSQLCommand()
                throws YASQTException
        {
            if (!line.endsWith(Constants.MULTIPLE_LINE_QUERY_INDICATOR))
                return line;

            String sqlCmd = "";
            do {
                try {
                    sqlCmd += getUsableLine();
                    advanceToNextUsableLine();
                } catch (IOException e) {
                    throw new YASQTException(YASQTException.Type.UNKNOWN, "Previous line ended with '" + Constants.MULTIPLE_LINE_QUERY_INDICATOR + "' but seems to be no more lines", e);
                }
            } while (
                    line != null &&
                            line.endsWith(Constants.MULTIPLE_LINE_QUERY_INDICATOR)
                    );
            if (line != null && !line.startsWith(Constants.COMMENT_INDICATOR))
                sqlCmd += getUsableLine();

            return sqlCmd;
        }

        private String getUsableLine() {
            return line.endsWith(Constants.MULTIPLE_LINE_QUERY_INDICATOR) ?
                    line.substring(0, line.lastIndexOf(Constants.MULTIPLE_LINE_QUERY_INDICATOR)) :
                    line;
        }

        /**
         * Method not implemented
         */
        public void remove() {
            // Do Nothing
        }

    }
}
