package ar.com.bitman.yasqt.commands;

import ar.com.bitman.yasqt.YASQTException;

public abstract class Command {

    protected String args;

    public abstract void execute() throws YASQTException;

    public abstract void displayResults() throws YASQTException;

    public void setArgs(String args) {
        this.args = args;
    }
}
