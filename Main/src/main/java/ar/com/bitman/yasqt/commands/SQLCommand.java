package ar.com.bitman.yasqt.commands;

import ar.com.bitman.yasqt.Logger;
import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.model.Column;
import ar.com.bitman.yasqt.utils.StringHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SQLCommand
extends Command {
    public static final char CMD_SEPARATOR = ' ';
    private static final String COLUMN_SEPARATOR = "  ";
    private static final int COLUMN_SEPARATION_DISTANCE = 3;

    private String sqlCmd;

    private static Set<String> dml = new HashSet<>();
    private static Set<String> ddl = new HashSet<>();
    private static Set<String> yasqtCmd = new HashSet<>();

    private boolean isDML;
    private boolean isDDL;
    private boolean isYasqtCmd;

    private Connection conn = null;
    private ResultSet rs;
    private int affectedRows;

    static {
        dml.add("update");
        dml.add("insert");
        dml.add("delete");
        dml.add("truncate");

        ddl.add("drop");
        ddl.add("create");
        ddl.add("alter");
        ddl.add("rename");

        yasqtCmd.add("fields");
    }

    private List<Column> columns;

    public SQLCommand(String sqlCmd)
            throws YASQTException
    {
        this.sqlCmd = sqlCmd;
        int cmdIndex = sqlCmd.indexOf(CMD_SEPARATOR);
        if (cmdIndex == -1 )
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid SQL sentence '" + sqlCmd + "'");

        String cmd = sqlCmd.toLowerCase().substring(0, cmdIndex);
        isDML = dml.contains(cmd);
        isDDL = ddl.contains(cmd);
        isYasqtCmd = yasqtCmd.contains(cmd);
    }

    public SQLCommand(String sqlCmd, Connection conn)
            throws YASQTException
    {
        this(sqlCmd);
        this.conn = conn;
    }

    public String getSqlCmd() {
        return sqlCmd;
    }

    public boolean isDML() {
        return this.isDML;
    }

    public boolean isDDL() {
        return isDDL;
    }

    public boolean isYasqtCmd() {
        return isYasqtCmd;
    }

    public void execute() throws YASQTException {

        Logger.logInfo("Executing : " + getSqlCmd());

        try {
            Statement statement = conn.createStatement();

            if (hasResultSet()) {
                rs = statement.executeQuery(getSqlCmd());
            } else {
                affectedRows = statement.executeUpdate(getSqlCmd());
            }
        } catch (SQLException e) {
            throw new YASQTException(e, YASQTException.Type.UNKNOWN);
        }
    }


    public void setConn(Connection conn) {
        this.conn = conn;
    }

    private boolean hasResultSet() {
        return !(isDDL() || isDML());
    }
    public void displayResults() throws YASQTException {
        try {
            if (hasResultSet()) {
                printHeaders();

                while (rs.next())
                    printCurrentRow(rs);

            } else {
                Logger.logInfo("Row(s) affected : " + affectedRows);
            }
        } catch (SQLException e) {
            throw new YASQTException(e, YASQTException.Type.UNKNOWN);
        }
    }


    private void printHeaders() throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();
        // Print column names
        int i = 1;
        columns = getColumns();
        for (Column column : columns) {
            if (column.getName().length() > column.getDisplaySize()) {
                System.out.print(StringHelper.substring(column.getName(), column.getDisplaySize()));
            } else {
                final int blanks = column.getDisplaySize() - column.getName().length();
                System.out.print(column.getName() + StringHelper.repeat(' ', blanks + COLUMN_SEPARATION_DISTANCE));
            }
            i++;
        }

        System.out.println();

        // Print dashes lines
        i = 1;
        for (Column column : columns) {
            if (column.getName().length() > column.getDisplaySize()) {
                System.out.print(StringHelper.repeat('-', column.getDisplaySize()) + COLUMN_SEPARATOR);
            } else {
                System.out.print(StringHelper.repeat('-', column.getDisplaySize()) + StringHelper.repeat(' ', COLUMN_SEPARATION_DISTANCE));
            }
            i++;
        }
        System.out.println();


    }

    protected List<Column> getColumns()  {
        ResultSetMetaData metaData;
        List<Column> column = null ;

        try
        {
            metaData = rs.getMetaData();

        column = new ArrayList<>(metaData.getColumnCount());

        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            column.add(
                    new Column(metaData.getColumnName(i), metaData.getColumnDisplaySize(i))
            );
        }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return column;
    }


    private void printCurrentRow(ResultSet rs) {
        boolean lastColumn = false;
        for (int col = 1; !lastColumn; col++) {
            try {
                System.out.print(" " + rs.getString(col) + " --");
            } catch (SQLException e) {
                lastColumn = true;
                System.out.println("*");
            }
        }
    }
}