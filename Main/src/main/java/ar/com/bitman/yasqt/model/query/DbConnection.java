package ar.com.bitman.yasqt.model.query;

import ar.com.bitman.yasqt.Logger;
import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.config.ConfigManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.*;
import java.util.Properties;
import java.util.Set;

public class DbConnection
{
    private String connectionString;
    private final String userid;
    private final String password;
    private final String dbName;
    private final ConfigManager config;

    private boolean connected;
    private Connection conn;

    private Driver driver;

    public DbConnection(String connectionString, String userid, String password, String dbName, ConfigManager config)
    {
        this.connectionString = connectionString;
        this.userid = userid;
        this.password = password;
        this.dbName = dbName;
        this.connected = false;
        this.config = config;
    }

    public void setDriver(Driver driver)
    {
        this.driver = driver;
    }

    /**
     * Performs connection to database regardless of connecion already established
     */
    private void connect()
            throws YASQTException
    {
        // Adding a driver instead of modifying the classpath to use a new driver
        // extracted from : http://www.kfu.com/~nsayer/Java/dyn-jdbc.html

        if (config.isUrlShortcut(this.connectionString))
            this.connectionString = config.getConnectionString(this.connectionString);

        try
        {
            if ( driver != null) {
                registerDriver(driver);
            } else
            {
                loadDriverFromJAR();
            }

            Logger.logInfo("Connecting to " + this.connectionString);
            conn = DriverManager.getConnection(this.connectionString, this.userid, this.password);
        } catch (Throwable e)
        {
            throw new YASQTException(YASQTException.Type.CONNECTION, "Error connecting to " + this.connectionString, e);
        }

        connected = true;
        Logger.logInfo("Database connection established");
    }

    private void loadDriverFromJAR()
            throws IOException, YASQTException, ClassNotFoundException, IllegalAccessException, InstantiationException,
            SQLException
    {
        URLClassLoader urlClassLoader = createNewClassloader(this.dbName);
        registerNewDriver(this.dbName, urlClassLoader);
    }


    private URLClassLoader createNewClassloader(String db_name)
            throws  IOException,  YASQTException
    {
        Set<File> jarPath = config.getJarPaths(db_name);
        showJarPaths(jarPath);

        URL[] jarsUrl = new URL[jarPath.size()];

        int i = 0;
        for (File path : jarPath)
        {
            jarsUrl[i++] = path.toURI().toURL();
        }

        return new URLClassLoader(jarsUrl);
    }


    private void showJarPaths(Set<File> jarPath)
    {
        Logger.logDebug("jar files to load : ");

        for (File jar : jarPath)
            Logger.logDebug(jar.toString());

    }


    private void registerNewDriver(String db_name, URLClassLoader urlClassLoader)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException,
            SQLException, IOException, YASQTException
    {
        String driver = config.getJdbcProperties().getDriver(db_name);
        Logger.logDebug("JDBC driver name : " + driver);
        Driver d = (Driver) Class.forName(driver, true, urlClassLoader).newInstance();
        registerDriver(d);
    }

    private void registerDriver(Driver d)
            throws SQLException
    {
        DriverManager.registerDriver(new DriverShield(d));
    }

    public Connection getConnection()
            throws YASQTException
    {
        if (!this.connected)
        {
            connect();
        }
        return conn;
    }

    private class DriverShield
            implements Driver
    {
        private Driver driver;

        DriverShield(Driver d)
        {
            this.driver = d;
        }

        public boolean acceptsURL(String u)
                throws SQLException
        {
            return this.driver.acceptsURL(u);
        }

        public Connection connect(String u, Properties p)
                throws SQLException
        {
            return this.driver.connect(u, p);
        }

        public int getMajorVersion()
        {
            return this.driver.getMajorVersion();
        }

        public int getMinorVersion()
        {
            return this.driver.getMinorVersion();
        }

        public DriverPropertyInfo[] getPropertyInfo(String u, Properties p)
                throws SQLException
        {
            return this.driver.getPropertyInfo(u, p);
        }

        public boolean jdbcCompliant()
        {
            return this.driver.jdbcCompliant();
        }

        public java.util.logging.Logger getParentLogger()
                throws SQLFeatureNotSupportedException
        {
            throw new SQLFeatureNotSupportedException();
        }
    }
}
