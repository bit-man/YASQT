package ar.com.bitman.yasqt;

import ar.com.bitman.yasqt.config.ConfigManager;

public class Logger {


    public static void logInfo(String msg) {
        System.out.println(Constants.HEADER_INFO + msg);
    }
    public static void logDebug(String msg) {
        boolean debug = false;

        try {
            debug = (Boolean) ConfigManager.getInstance().getRuntimeProperty(Constants.DEBUG_PROPERTY);

        }  catch (YASQTException e)
        {
            e.printStackTrace();
        }

        if (debug)
                System.out.println(Constants.HEADER_DEBUG + msg);
    }
}
