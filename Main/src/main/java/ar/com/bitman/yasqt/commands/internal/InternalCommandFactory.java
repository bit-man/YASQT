package ar.com.bitman.yasqt.commands.internal;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.Command;

import java.util.HashMap;
import java.util.Map;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 9/6/13
 * Time: 4:26 PM
 */
public class InternalCommandFactory {
    private static Map<String,Command> internalCommand = new HashMap<>();

    static {
        internalCommand.put("shortcut-list", new ShortcutList());
        internalCommand.put("shortcut-show", new ShortcutShow());
    }

    public static Command command(String qry) throws YASQTException {
        String first = getFisrt(qry);

        if ( internalCommand.keySet().contains(first))   {
            Command command = internalCommand.get(first);
            command.setArgs(getArgs(qry).trim());
            return command;
        }
        else
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid internal command '" + qry + "'");
    }

    private static String getArgs(String qry) {
        String qryOK = qry.trim();
        int i = qryOK.indexOf(' ');
        return i == -1 ? "" : qry.substring(i);
    }

    private static String getFisrt(String qry) {
        String qryOK = qry.trim();
        int i = qryOK.indexOf(' ');
        return i == -1 ? qry : qry.substring(0,i);
    }

    public static boolean isCommand(String qry) {
        return internalCommand.keySet().contains(getFisrt(qry));
    }
}
