package ar.com.bitman.yasqt.config;
 

import ar.com.bitman.yasqt.YASQTException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JarPathProperties {

    private static JarPathProperties singleton;
    private List<File> jarPath;

    private JarPathProperties() {
    }

    public static JarPathProperties getInstance()
            throws YASQTException
    {
        if (singleton == null) {
            singleton = new JarPathProperties();
            singleton.load();
        }

        return singleton;
    }

    private void load()
            throws YASQTException
    {
        Properties bareProps = new Properties();
        try
        {
            bareProps.load(singleton.getClass().getResourceAsStream(Constants.JARPATH_PROPERTIES));
        } catch (IOException e)
        {
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Can't read file " + Constants.JARPATH_PROPERTIES, e);
        }

        for (String pName : bareProps.stringPropertyNames()) {
            if ("jarPathList".equals(pName)) {
                jarPath = new ArrayList<>();
                for ( String path : bareProps.getProperty(pName).split(",") )  {
                    path = Tools.replaceVariables(path);
                    jarPath.add(new File(path));
                }
            }
        }

    }

    public List<File> getPaths() {
        return jarPath;
    }
}
