package ar.com.bitman.yasqt.model;

public class Column {

    private String name;
    private int displaySize;

    public Column(String name, int displaySize) {
        this.name = name;
        this.displaySize = displaySize;
    }

    public int getDisplaySize() {
        return displaySize;
    }

    public String getName() {
        return name;
    }
}
