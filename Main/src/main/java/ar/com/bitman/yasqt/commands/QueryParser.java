package ar.com.bitman.yasqt.commands;

import ar.com.bitman.yasqt.commands.internal.InternalCommand;
import ar.com.bitman.yasqt.commands.internal.InternalCommandFactory;
import ar.com.bitman.yasqt.config.ConfigManager;

import java.io.File;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 9/6/13
 * Time: 3:45 PM
 */
public class QueryParser {
    private final ConfigManager config;

    private CommandType cmdType;

    public QueryParser(ConfigManager config) {
        this.config = config;
    }

    public void parse(String queryParam) {
        if (InternalCommandFactory.isCommand(queryParam))
            cmdType = CommandType.INTERNAL;
        else if (config.isQueryShortcut(queryParam))
            cmdType = CommandType.SHORTCUT;
        else if (new File(queryParam).exists())
            cmdType = CommandType.FILE;
        else
            cmdType = CommandType.SQL_QUERY;
    }

    public CommandType getCommandType() {
        return cmdType;
    }
}
