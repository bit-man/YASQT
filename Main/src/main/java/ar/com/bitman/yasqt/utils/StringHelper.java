package ar.com.bitman.yasqt.utils;

public class StringHelper {
    /**
     * Returns a new string that is a substring of this string. The
     * substring begins at string begin (index 0) and
     * extends to the character at index <code>lenght</code>.
     * Thus the length of the substring is <code>lenght</code>.
     * <p>
     * Examples:
     * <blockquote><pre>
     * substring("lamborghini", 4) returns "lamb"
     * </pre></blockquote>
     *
     * @param      lenght     the ending index, exclusive.
     * @return     the specified substring.
     * @exception  IndexOutOfBoundsException  if the
     *             <code>lenght</code> is larger than the length of
     *             this <code>String</code> object
     */
    public static String substring(String str, int lenght) {
        return str.substring( 0, lenght);
    }


    /***
     * Returns a string that contains the char <code>c</code> repeated <code>times</code> times
     * @param c
     * @param times
     * @return
     */
    public static String repeat( char c, int times) {
        String str = new String();
        for (int i = 0; i < times; i++)
            str += c;
        return str;
    }
}
