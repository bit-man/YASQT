package ar.com.bitman.yasqt.config.locator;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SimpleVisitor
        implements FileVisitor<Path> {

    private List<String> fileName;
    private Set<File> jarPaths;

    public SimpleVisitor(List<String> fileName, Set<File> jarPaths) {
        this.fileName = new ArrayList<String>(fileName);
        this.jarPaths = jarPaths;
    }

    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
        String jarFileName = path.getFileName().toFile().getPath();
        if (fileName.contains(jarFileName)) {
            jarPaths.add(path.toFile());
            fileName.remove(jarFileName);
        }

        return (fileName.size() == 0) ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
    }

    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
}
