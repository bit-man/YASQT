package ar.com.bitman.yasqt.config;

import ar.com.bitman.yasqt.YASQTException;

public class UrlProperties
extends ShortcutProperties{
    private static UrlProperties singleton;

    private UrlProperties()
            throws YASQTException
    {
        createShortcutsFile(Constants.JDBCURL_SHORTCUTS_PROPERTIES);
        load();
    }

    public static UrlProperties getInstance()
            throws YASQTException
    {
        if (singleton == null)
            singleton = new UrlProperties();

        return singleton;
    }

}
