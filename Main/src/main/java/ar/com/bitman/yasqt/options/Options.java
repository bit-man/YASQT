package ar.com.bitman.yasqt.options;


import ar.com.bitman.yasqt.YASQTException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Options {
    private String[] args;

    private HashMap<ValidOptions, List<String>> cmdArgs;

    public Options(String[] args) {
        this.args = args;
        this.cmdArgs = new HashMap<>();
    }

    public void parse() {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            arg = arg.substring(arg.lastIndexOf(Constants.DASH) + 1).toUpperCase();
            ValidOptions option = ValidOptions.valueOf(arg);
            List<String> cArgs = new ArrayList<>(option.getNumArgs());

            for (int j = 0; j < option.getNumArgs(); j++)
                cArgs.add(args[++i]);

            cmdArgs.put(option, cArgs);

        }
    }

    public String getArg(ValidOptions option, int argNum) throws YASQTException {
        if (!cmdArgs.keySet().contains(option))
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Option " + option + " not passed as argument");
        return cmdArgs.get(option).get(argNum);
    }

    public boolean hasOption(ValidOptions option) {
        return cmdArgs.containsKey(option);
    }
}

