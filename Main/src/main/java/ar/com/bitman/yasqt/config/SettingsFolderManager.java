package ar.com.bitman.yasqt.config;

import ar.com.bitman.yasqt.YASQTException;

import java.io.*;

public class SettingsFolderManager {

    private static SettingsFolderManager singleton;

    private File settingsPath;

    private SettingsFolderManager()
            throws YASQTException
    {
        createSettingsFolder();
    }

    public static SettingsFolderManager getInstance()
            throws YASQTException
    {
        if (singleton == null)
            singleton = new SettingsFolderManager();

        return singleton;
    }


    private void createSettingsFolder()
            throws YASQTException
    {
        settingsPath = new File(System.getenv("HOME"), Constants.SETTINGS_FOLDER);

        if (!settingsPath.exists())
            if (! settingsPath.mkdir())
                throw new YASQTException(YASQTException.Type.UNKNOWN, "Can't create folder " + settingsPath.getAbsolutePath());
    }


    File getSettingsPath() {
        return settingsPath;
    }


}
