package ar.com.bitman.yasqt.config;

public class Constants {
    static final String JDBC_PROPERTIES = "jdbc.properties";
    static final String JARPATH_PROPERTIES = "jarPath.properties";
    static final String JDBCURL_SHORTCUTS_PROPERTIES = "jdbcShortcuts.properties";
    static final String QUERY_SHORTCUTS_PROPERTIES = "queryShortcuts.properties";

    public static final String SETTINGS_FOLDER = ".yasqt";

    static final String DOT = ".";
    public static final String HOME_FOLDER = "${HOME}";
    public static final String JARS = ".jars";
    public static final String DRIVER = ".driver";
}
