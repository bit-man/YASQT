package ar.com.bitman.yasqt;

import ar.com.bitman.yasqt.commands.QueryFile;
import ar.com.bitman.yasqt.commands.QueryParser;
import ar.com.bitman.yasqt.commands.SQLCommand;
import ar.com.bitman.yasqt.commands.internal.InternalCommand;
import ar.com.bitman.yasqt.config.ConfigManager;
import ar.com.bitman.yasqt.model.query.DbConnection;
import ar.com.bitman.yasqt.options.Options;
import ar.com.bitman.yasqt.options.ValidOptions;
import ar.com.bitman.yasqt.utils.YASQTCommand;

import java.util.Iterator;

/**
 * Yet Another SQL Query Tool
 * <p/>
 * Everything started from this Java example : http://www.akadia.com/services/sqlsrv_jdbc.html
 */
public class YASQT {

    private final DbConnection db;
    private static ConfigManager config;

    public YASQT(String connectionString, String userid, String password, String dbName)
            throws YASQTException
    {
        this(connectionString, userid, password, dbName, false);
    }

    public YASQT(String connectionString, String userid, String password, String dbName, boolean debug)
            throws YASQTException
    {
        config = ConfigManager.getInstance();
        config.setRuntimeProperty(Constants.DEBUG_PROPERTY, debug);
        this.db = new DbConnection(connectionString,userid,password,dbName,config);
    }

    public void query(String queryParam)
            throws YASQTException
    {

        QueryParser queryParser = new QueryParser(config);
        queryParser.parse(queryParam);

        switch (queryParser.getCommandType()) {
            case INTERNAL:
                InternalCommand internalCommand = new InternalCommand(queryParam);
                internalCommand.execute();
                internalCommand.displayResults();
                break;

            case SHORTCUT:
                execQuery(new SQLCommand(config.getQuery(queryParam), db.getConnection()));
                break;

            case SQL_QUERY:
                final SQLCommand sqlCommand = new SQLCommand(queryParam);
                sqlCommand.setConn(db.getConnection());
                execQuery(sqlCommand);
                break;

            case FILE:                QueryFile qFile = new QueryFile(queryParam);
                Iterator<SQLCommand> sqlCommands = qFile.getIterator();

                while (sqlCommands.hasNext()) {
                    SQLCommand nextSqlCommand = sqlCommands.next();
                    nextSqlCommand.setConn(db.getConnection());
                    execQuery(nextSqlCommand);
                }
                break;
        }
    }

    private void execQuery(SQLCommand qry)
            throws  YASQTException
    {
        if ( qry.isYasqtCmd() )
            execYASQTcommand(qry);
        else
            execSQLcommand(qry);

    }
    private void execYASQTcommand(SQLCommand yasqtCommand)
            throws YASQTException
    {
        final String yasqtCommandSqlCmd = yasqtCommand.getSqlCmd();
        if ( yasqtCommandSqlCmd.toLowerCase().startsWith("fields"))   {
            String tableName = yasqtCommandSqlCmd.substring( yasqtCommandSqlCmd.indexOf(' '), yasqtCommandSqlCmd.length());
            String sqlCmd = "select * from " + tableName;

            final YASQTCommand yCommand = new YASQTCommand(sqlCmd, db.getConnection());
            yCommand.execute();
            yCommand.displayColumnNames();
        } else
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Invalid YASQT command : '" + yasqtCommand + "'");
    }


    private void execSQLcommand(SQLCommand sqlCommand) throws YASQTException {
        sqlCommand.execute();
        sqlCommand.displayResults();
    }

    public static void main(String[] args)
            throws YASQTException

    {

        Options opt = new Options(args);
        opt.parse();

        if (!(opt.hasOption(ValidOptions.USER) && opt.hasOption(ValidOptions.URL) &&
                opt.hasOption(ValidOptions.PWD) && opt.hasOption(ValidOptions.DBNAME) &&
                opt.hasOption(ValidOptions.QUERY))) {
            showHelp();
            System.exit(-1);
        }
        final String connectionString = opt.getArg(ValidOptions.URL, 0);
        final String userid = opt.getArg(ValidOptions.USER, 0);
        final String password = opt.getArg(ValidOptions.PWD, 0);
        final String dbName = opt.getArg(ValidOptions.DBNAME, 0);
        final String queryParam = opt.getArg(ValidOptions.QUERY, 0);

        YASQT yasqt = new YASQT(connectionString, userid, password, dbName, opt.hasOption(ValidOptions.DEBUG));

        yasqt.query(queryParam);
    }

    private static void showHelp() {
        final String name = YASQT.class.getCanonicalName();
        System.out.println("\n" +
                "usage:\n" +
                "        " + name + " --url jdbcURL --user dbUser --pwd dbPassword\n" +
                "                                  --dbName databaseName --query queryToExecute\n" +
                "                                  [--debug] [-h|--help]\n\n" +
                "url   : JDBC URL or URL shortcut\n" +
                "user  : database user\n" +
                "pwd   : password for database user\n" +
                "query : SQL query, query file path or query shortcut\n" +
                "debug : shows debugging information (useful for bug reporting)\n" +
                "help  : this help\n" +
                "\n" +
                "For more help take a look at https://github.com/bit-man/YASQT/wiki/YasqtHelp\n" +
                "\n" +
                "e.g. :\n\n" +
                "    " + name + " --url jdbc:mysql://localhost:3306/ --user root --pwd s3cr3t\n" +
                "             --dbName test --query 'select count(*) from table' \n\n" +
                "    " + name + " --url mysql --user root --pwd s3cr3t\n" +
                "             --dbName test --query countTable\n"
        );
    }
}

