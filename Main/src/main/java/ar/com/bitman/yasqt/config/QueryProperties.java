package ar.com.bitman.yasqt.config;

import ar.com.bitman.yasqt.YASQTException;

public class QueryProperties
    extends ShortcutProperties {
    private static QueryProperties singleton;


    private QueryProperties()
            throws YASQTException
    {
        createShortcutsFile(Constants.QUERY_SHORTCUTS_PROPERTIES);
        load();
    }

    public static QueryProperties getInstance()
            throws YASQTException
    {
        if (singleton == null)
            singleton = new QueryProperties();

        return singleton;
    }

}
