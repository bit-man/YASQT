package ar.com.bitman.yasqt.config;

import ar.com.bitman.yasqt.YASQTException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public abstract class ShortcutProperties {
    protected Map<String, String> shortcut;
    protected File shortcutsPath;

    public String getShortcut(String shortCut) {
        return shortcut.get(shortCut);
    }

    public boolean isShortcut(String shortcut) {
        return this.shortcut.keySet().contains(shortcut);
    }

    protected void load()
            throws YASQTException
    {
        Properties bareProps = new Properties();
        try
        {
            bareProps.load(new FileInputStream(shortcutsPath));
        } catch (IOException e)
        {
            throw new YASQTException(YASQTException.Type.UNKNOWN, "Cna't load file " + shortcutsPath,e);
        }

        shortcut = new HashMap<>();
        for ( String pName :  bareProps.stringPropertyNames()) {
            shortcut.put(pName, (String) bareProps.get(pName));
        }
    }

    public void load( Map<String,String> map) {
        for ( String pName :  map.keySet()) {
            shortcut.put(pName, map.get(pName));
        }
    }


    protected void createShortcutsFile(final String queryShortcutsProperties)
            throws YASQTException
    {
        shortcutsPath = new File(SettingsFolderManager.getInstance().getSettingsPath(), queryShortcutsProperties);

        if ( ! shortcutsPath.exists())
            try
            {
                if ( ! shortcutsPath.createNewFile() )
                    throw new YASQTException(
                            YASQTException.Type.UNKNOWN, "Can't create file " + shortcutsPath.getAbsolutePath()
                    );
            } catch (IOException e)
            {
                throw new YASQTException(YASQTException.Type.UNKNOWN, "Can't create file " + shortcutsPath, e);
            }
    }

    public Set<String> getShortcutList() {
        return shortcut.keySet();
    }
}
