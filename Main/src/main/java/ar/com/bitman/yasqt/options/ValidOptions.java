package ar.com.bitman.yasqt.options;

public enum ValidOptions {
    URL(1),
    USER(1),
    PWD(1),
    DBNAME(1),
    QUERY(1),
    DEBUG(0),
    HELP(0),
    H(0);

    public int getNumArgs() {
        return numArgs;
    }

    private int numArgs;

    ValidOptions(int numArgs) {
        this.numArgs = numArgs;
    }
}
