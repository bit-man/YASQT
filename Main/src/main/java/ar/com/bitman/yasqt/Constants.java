package ar.com.bitman.yasqt;

public class Constants {
    public static final String COMMENT_INDICATOR = "#";
    public static final String MULTIPLE_LINE_QUERY_INDICATOR = "\\";
    public static final String HEADER_INFO = "[INFO] ";
    public static final String HEADER_DEBUG = "[DEBUG] ";
    public static final String DEBUG_PROPERTY = "debug";
}
