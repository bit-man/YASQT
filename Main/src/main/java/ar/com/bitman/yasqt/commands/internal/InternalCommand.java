package ar.com.bitman.yasqt.commands.internal;

import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.commands.Command;

public class InternalCommand extends Command {
    private final String qry;
    private Command cmd;

    public InternalCommand(String queryParam) {
        this.qry = queryParam;
    }

    public void execute()
            throws YASQTException
    {
        cmd = InternalCommandFactory.command(qry);
        cmd.execute();
    }

    public void displayResults() throws YASQTException {
        cmd.displayResults();
    }

}
