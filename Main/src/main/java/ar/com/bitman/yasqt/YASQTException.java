package ar.com.bitman.yasqt;

public class YASQTException
extends Exception {
    private Type type;

    public YASQTException(Exception e, Type type) {
        super(e);
        this.type = type;
    }

    public YASQTException(Type type, String s) {
        super(s);
        this.type = type;
    }

    public YASQTException(Type type, String s, Throwable e) {
        super(s,e);
        this.type = type;
    }

    public Type getType()
    {
        return type;
    }

    public enum Type
    {
        UNKNOWN, CONNECTION
    }
}
