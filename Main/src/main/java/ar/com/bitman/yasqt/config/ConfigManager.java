package ar.com.bitman.yasqt.config;


import ar.com.bitman.yasqt.YASQTException;
import ar.com.bitman.yasqt.config.locator.SimpleVisitor;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class ConfigManager {

    private static ConfigManager singleton;
    private final JarPathProperties jarPathProperties;
    private final JdbcProperties jdbcProperties;
    private final SettingsFolderManager settingsFolderManager;
    private File settingsPath;
    private final UrlProperties urlProperties;
    private QueryProperties queryProperties;
    private Properties runtimeProperties;

    private ConfigManager()
            throws YASQTException
    {
        jarPathProperties = JarPathProperties.getInstance();
        jdbcProperties = JdbcProperties.getInstance();
        settingsFolderManager = SettingsFolderManager.getInstance();
        urlProperties = UrlProperties.getInstance();
        queryProperties = QueryProperties.getInstance();
        runtimeProperties = new Properties();
    }

    public static ConfigManager getInstance()
            throws YASQTException
    {
        if (singleton == null)
            singleton = new ConfigManager();

        return singleton;
    }

    public JdbcProperties getJdbcProperties()
            throws YASQTException
    {
        return JdbcProperties.getInstance();
    }


    public Set<File> getJarPaths(String dbName)
            throws IOException, YASQTException
    {
        List<String> jarNames = jdbcProperties.getJarNames(dbName);
        Set<File> jarPaths = new HashSet<>();

        for ( File path : jarPathProperties.getPaths()) {
            Path startPath = FileSystems.getDefault().getPath(path.getAbsolutePath());

            FileVisitor<Path> visitor = new SimpleVisitor(jarNames, jarPaths);
            Files.walkFileTree(startPath, visitor);

            if ( jarPaths.size() == jarNames.size())
                return jarPaths;
        }

        // ToDo add list of not found jar files
        throw new YASQTException(YASQTException.Type.UNKNOWN, "Some jar files weren't found ");
    }

    public boolean isUrlShortcut(String connectionString) {
        return urlProperties.isShortcut(connectionString);
    }

    public String getConnectionString(String shortcut) {
        return urlProperties.getShortcut(shortcut);
    }

    public boolean isQueryShortcut(String connectionString) {
        return queryProperties.isShortcut(connectionString);
    }

    public String getQuery(String shortcut) {
        return queryProperties.getShortcut(shortcut);
    }

    public QueryProperties getQueryManager() {
        return queryProperties;
    }

    public void setRuntimeProperty(String key, Object value) {
          runtimeProperties.put(key,value);
    }

    public Object getRuntimeProperty(String key) {
        return runtimeProperties.get(key);
    }
}
